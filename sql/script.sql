USE [RecursosHumanos]
GO
IF OBJECT_ID('dbo.EMPLOYEE', 'U') IS NOT NULL
	DROP TABLE dbo.EMPLOYEE;
GO
/****** Object:  Table [dbo].[EMPLOYEE]    Script Date: 15/10/2020 19:23:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EMPLOYEE](
	[fiId] [int] PRIMARY KEY IDENTITY(1,1),
	[fcName] [varchar](50) NOT NULL,
	[fiAge] [int] NOT NULL,
	[fdBirthdate] [datetime] NOT NULL,	
	[fbEnabled] [bit] NOT NULL,
	[fdLastupdate] [datetime] NOT NULL);
GO
SET ANSI_PADDING OFF
GO