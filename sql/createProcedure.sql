USE RecursosHumanos
GO
IF OBJECT_ID('dbo.spDeleteEmployee', 'P') IS NOT NULL
	DROP PROC dbo.spDeleteEmployee;

GO

CREATE PROCEDURE dbo.spDeleteEmployee (@piId int) 
AS

SET NOCOUNT ON

DECLARE @vcMessage varchar(200)

IF EXISTS(SELECT fiId FROM dbo.EMPLOYEE WITH(NOLOCK) WHERE fiId=@piId)
BEGIN
	UPDATE dbo.EMPLOYEE set fbEnabled = 0 WHERE fiId=@piId
	IF @@ERROR <> 0
	BEGIN
		SET @vcMessage = 'Error al intentar actualizar la tabla EMPLOYEE'
		GOTO ERRORES
	END
	SET NOCOUNT OFF
	RETURN 0
END

ERRORES:
	SET NOCOUNT OFF
	IF @vcMessage IS NULL SET @vcMessage='Error al ejecutar spDeleteEmployee'
	RAISERROR (@vcMessage, 18, -2)
	RETURN -1