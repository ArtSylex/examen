USE RecursosHumanos
GO
IF OBJECT_ID('dbo.spConEmployees', 'P') IS NOT NULL
	DROP PROC dbo.spConEmployees;

GO

CREATE PROCEDURE dbo.spConEmployees AS
BEGIN
	SELECT fiId, fcName, fiAge, fdBirthdate, fbEnabled, fdLastupdate FROM dbo.EMPLOYEE WHERE fbEnabled = 1;
END;