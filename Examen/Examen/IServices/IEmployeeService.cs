﻿using Examen.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Examen.IServices
{
    public interface IEmployeeService
    {
        public ResponseModel Delete(int idEmployee);
        public ResponseModel Save(Employee employee);
        public List<Employee> GetEmployees();
    }
}
