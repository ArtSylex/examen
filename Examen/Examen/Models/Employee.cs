﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Examen.Models
{
    public class Employee
    {
        public int fiId { get; set; }
        public string fcName { get; set; }
        public int fiAge { get; set; }
        public DateTime fbBirthdate { get; set; }
        public bool fbEnabled { get; set; }
        public DateTime fdLastupdate { get; set; }
    }
}
