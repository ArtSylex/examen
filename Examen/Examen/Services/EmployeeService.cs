﻿using Examen.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Examen.IServices;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using Examen.Common;

namespace Examen.Services
{
    public class EmployeeService : IEmployeeService
    {
        public ResponseModel Delete(int idEmployee)
        {
            ResponseModel result = new ResponseModel();
            try
            {
                using(IDbConnection conn = new SqlConnection(Global.ConnectionString))
                {
                    if (conn.State == ConnectionState.Closed) conn.Open();
                    int execute = conn.Query<int>("spDeleteEmployee", this.SetParameters(idEmployee), commandType: CommandType.StoredProcedure).FirstOrDefault();
                    result.Code = execute;
                    result.Message = "Se eliminó correctamente.";
                }
            }
            catch (Exception ex)
            {
                result.Code = 500;
                result.Message = ex.Message;
            }
            return result;
        }

        public List<Employee> GetEmployees()
        {
            List<Employee> employees;
            try
            {
                using(IDbConnection conn = new SqlConnection(Global.ConnectionString))
                {
                    if (conn.State == ConnectionState.Closed) conn.Open();
                    employees = conn.Query<Employee>("spConEmployees", commandType: CommandType.StoredProcedure).ToList();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return employees;
        }

        public ResponseModel Save(Employee employee)
        {
            throw new NotImplementedException();
        }

        private DynamicParameters SetParameters(int idEmployee)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("@piId", idEmployee);

            return parameters;
        }
    }
}
